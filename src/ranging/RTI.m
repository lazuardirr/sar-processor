% RTI
% Extract time and range and process RTI
% 
% Parameters:
% @range Detected range pulses
% @time Time of the pulses
% @parameters Radar parameters
% @mode RTI mode [1 => without clutter rejection,
%   2 => 2-pulse cancelor clutter rejection,
%   3 => 2-pulse magnitude only cancelor clutter rejection]
%
% Return Value
% @x Ranges
% @y Time
% @img Processed RTI
function [x, y, img] = RTI(range, time, parameters, mode = 1)
    zpad = 8 * parameters.samplePerPulse / 2;
    R = linspace(0, parameters.maxRange, zpad);    

    [r, c] = size(range);

    if (mode == 1)
        disp("Processing RTI without clutter rejection..");
        velocity = dbv(ifft(range, zpad, 2));
        halfVelocity = velocity(:, 1:size(velocity, 2) / 2);
        maxVelocity = max(max(velocity));
    endif

    if (mode == 2 || mode == 3)
        bound = size(range,1);

        if (mode == 2)
            disp("Processing RTI with 2-pulse cancelor clutter rejection..");
            R1 = range(1:bound-1,:);
            R2 = range(2:bound,:);
            velocity = ifft(R2 - R1, zpad,2);            
        endif

        if (mode == 3)
            disp("Processing RTI with 2-pulse magnitude only cancelor clutter rejection..");
            for i=1:bound-1
                v1 = abs(ifft(range(i,:), zpad));
                v2 = abs(ifft(range(i+1,:), zpad));
                velocity(i,:) = v2 - v1;
            endfor        
        endif

        % for i = 1:size(velocity,1)
        %     velocity(i,:) = velocity(i,:).*R.^(3/2); %Optional: magnitude scale to range
        % end

        velocity = dbv(velocity);
        halfVelocity = velocity(:, 1:size(velocity, 2) / 2);
        maxVelocity = max(max(halfVelocity));

        clear bound;
    endif

    x = R;
    y = time;
    img = halfVelocity - maxVelocity;
endfunction
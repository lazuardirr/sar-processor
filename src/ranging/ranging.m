% RTI
% 
% Process Range vs. Time Intensity (RTI) plot
% Based on MIT IAP Radar Course 2011
%
% Parameters:
% @name Final image output name
function ranging(name = "ranging_image.jpg")    
    addpath("../../bootstrap");
    work_dir = bootstrap();

    disp("Reading wav file..");
    [sync, data, fs, nbits] = reader(strcat(work_dir, "../resources/ranging.wav"));

    disp("Loading parameters..");
    parameters = parameters(fs);
    parameters.resolution = parameters.c / (2*parameters.bandwidth);
    parameters.maxRange = parameters.resolution * parameters.samplePerPulse / 2;

    disp("Detecting pulses..");
    [range, time] = pulseDetector(data, sync, parameters, @parser, 1);

    disp("Substracting average..");
    range = range - mean(range, 1);

    disp("Processing RTI..")
    [x1, y1, img1] = RTI(range, time, parameters, 1);
    [x2, y2, img2] = RTI(range, time, parameters, 2);
    [x3, y3, img3] = RTI(range, time, parameters, 3);

    disp("Plotting Image..")
    figure(1);
    subplot(2,2,1);
    imagesc(x1, y1, img1, [-80,0]);
    colorbar;
    ylabel("time(s)");
    xlabel("range(m)");
    title("RTI w/o Clutter Rejection");

    subplot(2,2,2);
    imagesc(x2, y2, img2, [-80,0]);
    colorbar;
    ylabel("time(s)");
    xlabel("range(m)");
    title("RTI with 2-pulse Cancelor Clutter Rejection");

    subplot(2,2,3);
    imagesc(x3, y3, img3, [-20,0]);
    colorbar;
    ylabel("time(s)");
    xlabel("range(m)");
    title("RTI with 2-pulse mag only cancelor clutter rejection");

    print(gcf, "jpg", strcat("../../results/ranging/new_", name));
    disp("Finishing..");

    unload();
endfunction  
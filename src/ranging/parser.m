% parser
% parse detected raw data.
%
% Parameters:
% @idx Index of detected sync pulses.
% @data Raw range data.
% @parameters struct which holds radar parameters.
% 
% Return Value
% @range Range pulse
% @time Time of the pulse
function [range, time] = parser(idx, data, parameters)
    range = data(idx:idx+parameters.samplePerPulse - 1);
    time = idx / parameters.samplingFrequency;
endfunction
% pulseDetector
% Detect range data by looking for rising edge of sync pulses.
%
% Parameters:
% @data Raw range data.
% @sync Raw sync data.
% @parameters Struct which holds radar parameters.
% @parser Function to process detected range pulse.
% @mode Decide which detection algorithm to use
% 
% Return Value
% @range Detected range pulses
% @time Time of the pulses
function [range, time] = pulseDetector(data, sync, parameters, parser, mode = 1)
    count = 0;
    threshold = 0.08;
    % Generate logical matrix (1s and 0s) of sync data with magnitude greater than threshold.
    syncPulses = sync > threshold;
    range = zeros(1, parameters.samplePerPulse);

    % Modified Detection Algorithm 1
    % Processing time: 14-17s.
    % Parse data by iterating over rising edge indices extracted from syncPulses. 
    % This ensure to only iterate over rising edge samples (1s).
    if (mode == 1)
        rising = find(syncPulses > 0);
        i = 1;
        x = size(rising, 1); 
        bound = size(syncPulses, 1);

        while i < x
            index = rising(i);
            if (syncPulses(index) == 1 && % Ensure current sample are rising
                index - 11 > 0 && % Ignore first 10 samples
                index + parameters.samplePerPulse < bound && % Ensure current pulse samples within data bound 
                mean(syncPulses(index-11:index-1), 1) == 0) % Check whether last 10 samples is 0
                count++;
                % Assign current pulse and time using specific parser function.
                [range(count,:), time(count)] = parser(index, data, parameters);
            endif
            % Find next pulse rising index.
            i = find(rising >= index + parameters.samplePerPulse, 1, "first");
        endwhile
    endif

    % Modified Detection Algorithm 2
    % Processing time: 24-27s.
    % Parse data by iterating over each syncPulses but step over to next pulse sample (+ samplePerPulse)
    % Instead to next sample (+1)
    if (mode == 2)
        i = 100;
        x = size(syncPulses, 1) - parameters.samplePerPulse;

        while i < x
            if (syncPulses(i) == 1 && % Ensure current sample are rising
                mean(syncPulses(i-11:i-1)) == 0) % Check whether last 10 samples is 0
                count++;
                [range(count,:), time(count)] = parser(i, data, parameters);
                i = i + parameters.samplePerPulse;
            endif
            i++;
        endwhile
    endif

    % Original Detection Algorithm
    % Processing time: 190-200s.
    % Parse data by iterating over each syncPulses from 100th sample to N-samplePerPulse.
    if (mode == 3)
        clear i;
        for i = 100:(size(syncPulses,1)-parameters.samplePerPulse)
            if (syncPulses(i) == 1 && % Ensure current sample are rising
                mean(syncPulses(i-11:i-1)) == 0) % Check whether last 10 samples is 0
                count++;
                [range(count,:), time(count)] = parser(i, data, parameters);
            endif
        endfor
    endif
endfunction
function [rangeProfiles, syncProfiles] = positionDetector(data, sync, samplesPerRangeProfile, mode = 1)
    count = 0;

    syncPulses = abs(sync) > mean(abs(sync));
    rangeProfiles = zeros(1, samplesPerRangeProfile);
    syncProfiles = zeros(1, samplesPerRangeProfile);

    % Modified algorithm
    if (mode == 1)
        rising = find(syncPulses > 0);

        i = 1;
        x = size(rising, 1);

        while (i < x)
            idx = rising(i);
            
            if (syncPulses(idx) == 1 && 
                sum(syncPulses(idx-samplesPerRangeProfile:idx-1)) == 0)
                count++;
                rangeProfiles(count,:) = data(idx:idx+samplesPerRangeProfile-1);
                syncProfiles(count,:) = sync(idx:idx+samplesPerRangeProfile-1);
            endif

            i = find(rising > idx + samplesPerRangeProfile, 1, "first");
        endwhile
    endif

    % Original algorithm
    if (mode == 2)
        for i = samplesPerRangeProfile+1:size(syncPulses, 1)-samplesPerRangeProfile
            if ((syncPulses(i) == 1) &&
                sum(syncPulses(i-samplesPerRangeProfile:i-1)) == 0)
                count++;
                rangeProfiles(count,:) = data(i:i+samplesPerRangeProfile-1);
                syncProfiles(count,:) = sync(i:i+samplesPerRangeProfile-1);
            endif
        endfor
    endif
endfunction
function [Smf, Krr, Kxx] = matchedFilter(S, Rs, Kr, Kx)
    disp("Matched Filter..");
    clear i j;
    for i=1:size(S,2) % Iterate over row (time) to find phi_if
      for j=1:size(S,1) % Iterate over cross range in current time
        phiMf(j,i) = Rs * sqrt((Kr(i))^2 - (Kx(j))^2);
        Krr(j,i) = Kr(i);
        Kxx(j,i) = Kx(j);
      endfor  
    endfor  
    smf = exp(j*phiMf);
    Smf = S.*smf;

    clear smf phiMf;
endfunction
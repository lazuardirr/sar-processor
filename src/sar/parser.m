function s = parser(rangeProfiles, syncProfiles, samplePerPulse, thresh)
    count = 0;
    for j = 1:size(rangeProfiles,1)
        clear SIF;
        SIF = zeros(samplePerPulse,1);
        start = (syncProfiles(j,:)> thresh);
        startX = size(start, 2);
        count = 0;
        for i = 12:(size(start,2)-2*samplePerPulse)
            [Y I] =  max(syncProfiles(j,i:i+2*samplePerPulse));
            if mean(start(i-10:i-2)) == 0 && I == 1
                count = count + 1;
                SIF = rangeProfiles(j,i:i+samplePerPulse-1)' + SIF;
            end
        end
        % Hilbert transform
        q = ifft(SIF/count);
        s(j,:) = fft(q(size(q,1)/2+1:size(q,1)));
    end

    s(find(isnan(s))) = 1E-30; %set all Nan values to 0
endfunction
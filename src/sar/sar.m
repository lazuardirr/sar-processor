function s = sar(zpad = 2048, multiplier = 2, name = "final_image.jpg", plotProcess = 1)  
    tic();
    addpath("../../bootstrap");
    work_dir = bootstrap();

    disp("Reading wav file..");
    [sync, data, fs, nbits] = reader("../../resources/towardswarehouse.wav");

    disp("Loading parameters..");
    parameters = parameters(fs);
    parameters.samplesPerRangeProfile = parameters.rangeProfileDuration * parameters.samplingFrequency;
    
    figureCount = 1;

    % Detect positions
    disp("Detecting positions..");
    [rangeProfiles, syncProfiles] = positionDetector(data, sync, parameters.samplesPerRangeProfile, 1);
    clear data sync;

    % Parse data by pulse
    disp("Parsing pulse..");
    thresh = 0.08;
    s = parser(rangeProfiles, syncProfiles, parameters.samplePerPulse, thresh);

    % Radar parameters
    parameters.chripRate = parameters.bandwidth / 20E-3;
    Rs = 0;
    Xa = 0;
    deltaX = 2*(1/12)*0.3048;
    L = deltaX*(size(s,1));
    Xa = linspace(-L/2, L/2, (L/deltaX));
    Za = 0;
    Ya = Rs;
    t = linspace(0, parameters.pulseTime, size(s,2));
    Kr = linspace(((4*pi/parameters.c)*(parameters.frequency - parameters.bandwidth /2)), ((4*pi/parameters.c)*(parameters.frequency + parameters.bandwidth/2)), (size(t,2)));
   
    s = s.-mean(s,1);

    % Apply window
    disp("Windowing..");
    M = size(s,2); 
    H = hanning(M)';
    sh = s.*H;
    s = sh;
    clear sh;
    
    imgPrefix = strcat("_sar_", computer(), "_", mat2str(zpad), "_", mat2str(multiplier));
    set(0,'defaultaxesfontsize',13);
    
    if plotProcess == 1
      plotImage(angle(s), Kr, Xa, strcat(mat2str(figureCount), imgPrefix, "_", "phase_before_along_track_fft.jpg"), figureCount, 
          "Phase Before Along Track FFT", "K_r (rad/m)", "Synthetic Aperture Position, Xa (m)", "radians", [], gray);  
      figureCount++;    
    endif  

    % Along Track Fast Fourier Transform
    S = alongTrackFFT(s, zpad);
    clear s;

    Kx = linspace((-pi/deltaX), (pi/deltaX), size(S,1));
    
    if plotProcess == 1
      S_image = dbv(S);
      plotImage(S_image, Kr, Kx, strcat(mat2str(figureCount), imgPrefix, "_", "mag_after_along_track_fft.jpg"), figureCount, 
          "Magnitude After Along Track FFT", "K_r (rad/m)", "K_x (rad/m)", "dB", 
          [max(max(S_image))-40, max(max(S_image))], gray);  
      figureCount++; 
   
      S_image = angle(S);
      plotImage(S_image, Kr, Kx, strcat(mat2str(figureCount), imgPrefix, "_", "phase_after_along_track_fft.jpg"), figureCount, 
          "Phase After Along Track FFT", "K_r (rad/m)", "K_x (rad/m)", "radians", 
          [], gray);  
      figureCount++;   
    
      S_image = dbv(fftshift(fft(S, [], 2), 2));
      plotImage(S_image, Kr, Kx, strcat(mat2str(figureCount), imgPrefix, "_", "mag_after_2D_fft.jpg"), figureCount, 
          "Magnitude of 2-D FFT of Input Data", "R_{relative} (dimensionless)", "K_x (rad/m)", "dB", 
          [max(max(S_image))-40, max(max(S_image))], gray);  
      figureCount++; 
      
      clear S_image;
    endif  

    % Apply matched Filters
    [Smf, Krr, Kxx] = matchedFilter(S, Rs, Kr, Kx);
    clear S;
    
    if plotProcess == 1
        plotImage(angle(Smf), Kr, Kx, strcat(mat2str(figureCount), imgPrefix, "_", "phase_after_matched_filter.jpg"), figureCount, 
          "Phase After Matched Filter", "K_r (rad/m)", "K_x (rad/m)", "radians", 
          [], gray);  
        figureCount++; 
      
        S_image = dbv(fftshift(fft(Smf, [], 2), 2));
        plotImage(S_image, Kr, Kx, strcat(mat2str(figureCount), imgPrefix, "_", "mag_after_downrange_fft_of_matched_filtered_data.jpg"), figureCount, 
          "Magnitude of 2-D FFT of Matched Filtered Data", "R_{relative} (dimensionless)", "K_x (rad/m)", "dB", 
          [max(max(S_image))-40, max(max(S_image))], gray);  
        figureCount++; 
        
        clear S_image;
    endif  

    kstart = 73;
    kstop = 108.5;
    KyEven = linspace(kstart, kstop, 1024);

    % Perform stolt Interpolation
    Sst = stoltInterpolation(Smf, KyEven, Kr, Kx, zpad);
    clear Smf;
    
    if plotProcess == 1
        plotImage(angle(Sst), Kr, Kx, strcat(mat2str(figureCount), imgPrefix, "_", "phase_after_stolt_interpolation.jpg"), figureCount, 
          "Phase After Stolt Interpolation", "K_y (rad/m)", "K_x (rad/m)", "radians", 
          [], gray);  
        figureCount++; 
    endif  

    % Another windowing
    disp("Windowing...");
    N = size(KyEven, 2);
    H = hanning(N)';
    Ssth = Sst.*H;
    clear v Kr Krr Kxx KyEven Sst;

    % Perform inverse FFT's
    disp("Performing inverse FFT..");
    v = ifft2(Ssth, (size(Ssth,1)*multiplier), (size(Ssth,2)*multiplier)); %out of memory 

    % Truncate Data
    [truncImage, crossRange, downRange] = dataTruncation(v, Ssth, kstart, kstop, zpad, Rs, deltaX);
    truncImage = dbv(truncImage);

    % Plot and save final image
    plotImage(truncImage, crossRange, downRange, strcat(mat2str(figureCount), imgPrefix, "_", strrep(mat2str(toc()), '.', ','), "_", name), figureCount, 
        "Final Image", "Crossrange (ft)", "Downrange (ft)", "dB", 
        [max(max(truncImage))-40, max(max(truncImage))-0], 'default', 1);
    disp("Finishing..");

    unload();
endfunction 
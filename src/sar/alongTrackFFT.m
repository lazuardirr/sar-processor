function S = alongTrackFFT(s, zpad)
    % Along track FFT (in the slow time domain)
    % Symmetrically cross range zero pad for that the radar can squint
    disp("Processing along track FFT..");
    % avoiding loops, using vectorization for performance consideration.
    paddingTop = zeros(round((zpad - size(s,1)) / 2), size(s, 2));
    paddingBottom = zeros(zpad - size(s,1) - size(paddingTop), size(s,2));
    szeros = [paddingTop; s; paddingBottom];
    s = szeros;
    clear i index szeros;

    S = fftshift(fft(s, [], 1), 1);
endfunction
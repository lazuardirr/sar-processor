function plotImage(image, crossRange, downRange, name = "final_image.jpg", figureCount = 1, figureTitle = "Final Image", labelX = "Crossrange (ft)", labelY = "Downrange (ft)", cBarTitle = "dB", cLimit = [], cmap = 'default', axisEqual = 0)
    figure(figureCount);

    if isnull(cLimit)
        imagesc(crossRange, downRange, image);
    else
        imagesc(crossRange, downRange, image, cLimit);
    endif

    colormap(cmap);
    title(figureTitle);
    ylabel(labelY);
    xlabel(labelX);
    if axisEqual == 1
        axis equal;
    endif
    cbar = colorbar;
    set(get(cbar, "Title"), "String", cBarTitle, "fontsize", 13);
    print(gcf, "jpg", strcat("../../results/sar/", name));
endfunction
function Sst = stoltInterpolation(Smf, KyEven, Kr, Kx, zpad)
    count = 0;
    for i=1:zpad
        count++;
        Ky(count,:) = sqrt(Kr.^2 - Kx(i)^2);
        Sst(count,:) = (interp1(Ky(count,:), Smf(i,:), KyEven));
    endfor  

    Sst(find(isnan(Sst))) = 1E-30; % Set all NaN values to 0

    clear Ky;
endfunction
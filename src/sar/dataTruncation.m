function [truncImage, crossRange, downRange] = dataTruncation(v, Ssth, kstart, kstop, zpad, Rs, deltaX)
    bandwidth = 3E8 * (kstop - kstart) / (4*pi);
    maxRange = (3E8 * size(Ssth,2) / (2*bandwidth))*1/.3048;
    SImage = v;
    SImage = fliplr(rot90(SImage));
    cr1 = -80;
    cr2 = 80;
    dr1 = 1 + Rs / .3048;
    dr2 = 350 + Rs / .3048;
    % Data truncation
    drIndex1 = round((dr1/maxRange)*size(SImage,1));
    drIndex2 = round((dr2/maxRange)*size(SImage,1));
    crIndex1 = round(((cr1+zpad*deltaX/(2*.3048)) / (zpad*deltaX/.3048)) * size(SImage,2));
    crIndex2 = round(((cr2+zpad*deltaX/(2*.3048)) / (zpad*deltaX/.3048)) * size(SImage,2));
    truncImage = SImage(drIndex1:drIndex2, crIndex1:crIndex2);
    downRange = linspace(-1*dr1, -1*dr2, size(truncImage, 1)) + Rs/.3048;
    crossRange = linspace(cr1, cr2, size(truncImage, 2));

    truncImage = (truncImage'.*(abs(downRange*.3048)).^(3/2)).';
endfunction
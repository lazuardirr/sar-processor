% dbv
% Decible converter.
%
% Parameters:
% @in original value.
%
% Return Value
% @out value in decible.
function out = dbv(in)
    out = 20 * log10(abs(in));
endfunction
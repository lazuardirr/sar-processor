% reader
% Read audio file.
%
% Parameters:
% @file Audio File location.
% 
% Return Value
% @left Left channel data
% @right Right channel data
% @fs Sampling Frequency
% @nbits Bits Per Sample
function [left, right, fs, nbits] = reader(file) 
    if (nargin == 0)
        error("Invalid Arguments: file must be specified.")
    endif

    if (!ischar(file))
        error("Invalid Arguments: invalid file path.")
    endif
    
    try
        [y,f] = audioread(file);
    catch err
        error(err.message);
    end_try_catch

    % Invert audio samples
    left = -1*y(:,1);
    right = -1*y(:,2);

    fs = f;
    nbits = audioinfo(file).BitsPerSample;
endfunction
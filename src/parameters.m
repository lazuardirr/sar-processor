% parameters
% Generate a struct object which holds radar parameters.
%
% Parameters:
% @samplingFrequency
% @pulseTime
% @frequency
% @bandwidth
% rangeProfileDuration
%
% Return Value
% @parameters struct object which holds radar parameters.
function parameters = parameters(
    samplingFrequency = 44100,
    pulseTime = 20E-3,
    frequency = 2425E6,
    bandwidth = 330E6,
    rangeProfileDuration = 0.25
)
    parameters.c = 3E8;
    parameters.samplingFrequency = samplingFrequency;
    parameters.frequency = frequency;
    parameters.pulseTime = pulseTime;
    parameters.samplePerPulse = pulseTime * samplingFrequency;    
    parameters.lowCutOff = frequency - bandwidth/2;
    parameters.upperCutOff = frequency + bandwidth/2;
    parameters.bandwidth = bandwidth;
    parameters.rangeProfileDuration = rangeProfileDuration;
endfunction
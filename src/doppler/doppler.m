function doppler(name = "doppler_image.jpg")    
    addpath("../../bootstrap");
    work_dir = bootstrap();

    disp("Reading wav file..");
    [sync, data, fs, nbits] = reader(strcat(work_dir, "../resources/doppler.wav"));

    disp("Loading parameters..");
    c = 3E8;
    pulseTime = 0.250;
    samplePerPulse = pulseTime * fs;
    fc = 2590E6;

    totalSamples = round(size(data,1)/samplePerPulse)-1;

    disp("Parsing pulses..");
    sif = reshape(data(1:totalSamples*samplePerPulse, 1), samplePerPulse, totalSamples)' - mean(data);

    zpad = 8 * samplePerPulse / 2;

    disp("Calculating velocity..");
    v = dbv(ifft(sif, zpad, 2));
    v = v(:, 1:size(v,2)/2);
    mmax = max(max(v));
    deltaF = linspace(0, fs/2, size(v,2)); % (Hz)
    lambda = c/fc;
    velocity = deltaF * lambda / 2;

    disp("Calculating time..");
    time = linspace(1, pulseTime * size(v,1), size(v,1)); % (sec)

    disp("Plotting DTI..");
    figure(1);
    imagesc(velocity, time, v-mmax, [-35, 0]);
    colorbar;
    xlim([0 40]);
    xlabel('Velocity (m/s)');
    ylabel('time (s)');

    print(gcf, "jpg", "../../results/doppler/new_doppler.jpg");
    disp("Finishing..");

    unload();
endfunction
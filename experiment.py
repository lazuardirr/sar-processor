import sys
import os
import subprocess
import signal
import time

def sar(params):
    return ['cd src/sar;sar(%s);' % params, 'sar']

def doppler(params):
    return ['cd src/doppler;doppler(%s);' % params, 'doppler']

def ranging(params):
    return ['cd src/ranging;ranging(%s);' % params, 'ranging']

def rma(params):
    return ['cd old/sar;SBAND_RMA_opendata;', 'rma']

def dopplerOri(params): 
    return ['cd old/doppler;doppler;', 'doppler']

def rangingOri(params):
    return ['cd old/ranging;ranging;', 'ranging']

options = {
    0: sar,
    1: doppler,
    2: ranging,
    3: rma,
    4: dopplerOri,
    5: rangingOri
}

if __name__ == '__main__':
    if not len(sys.argv) > 2:
        print("Usage: %s mode [fileOutput] [experiment arguments]" % sys.argv[0])
        print("mode: 0 => sar, 1 => doppler, 2 => ranging, 3 => original sar, 4 => original doppler, 5 => original ranging")
        print("experiment arguments:")
        print("=> sar: [zpad = 2048], [multiplier = 4], [name = final_image.jpg], [plotProcess = 1]")
        exit(2)

    mode = int(sys.argv[1])

    if mode > len(options) - 1:
        print("Invalid experiment mode. [0 => sar, 1 => doppler, 2 => ranging, 3 => original sar, 4 => original doppler, 5 => original ranging]")
        exit(2)

    experiment = options[mode](','.join(sys.argv[3:]) if len(sys.argv) > 3 else '')

    try:
        octave = subprocess.Popen(['octave', '-qf', '--eval', experiment[0]])
        time.sleep(1)
        logger = subprocess.Popen(['python', 'log.py', str(octave.pid)], universal_newlines=True, stdout=open('results/' + experiment[1] + '/' + sys.argv[2], 'a') if len(sys.argv) > 2 else None)
        octave.wait()
    except KeyboardInterrupt:
        print("Cancelled.")
    except Exception as e:
        print(e)
    finally:
        if octave.poll() > 0:
            octave.kill()
        
        if logger.poll() > 0:
            logger.kill()
        
        print("Exiting..")
        exit(2)
import time
import string
import sys
import commands
import os

def get_cpumem(pid):
	d = [i for i in commands.getoutput("ps aux").split("\n")
		if i.split()[1] == str(pid)]
	return (float(d[0].split()[2]), float(d[0].split()[3]), d[0].split()[9].strip()) if d else (None, None, None)

def get_cpumemByCommand(cmd):
	d = [i for i in commands.getoutput("ps aux").split("\n")
		if str(cmd) in i.split()[10]]
	return (float(d[0].split()[2]), float(d[0].split()[3]), d[0].split()[9].strip()) if d else (None, None, None)

def get_temp():
	temp = os.popen("vcgencmd measure_temp").readline()
	return (temp.replace("temp=", "").strip())

if __name__ == '__main__':
	print("octave PID: %s" % sys.argv[1])
	if not len(sys.argv) >= 2 or (not all(i in string.digits for i in sys.argv[1]) and not sys.argv[1].isalpha()):
		print("usage: %s PID" % sys.argv[0])
		print("usage: %s Command" % sys.argv[0])
		exit(2)
	print('%CPU,%MEM,TEMP,TIME')
	try:
		while True:
			cpu,mem,elapsedTime = get_cpumemByCommand(sys.argv[1]) if sys.argv[1].isalpha() else get_cpumem(sys.argv[1])
			if not cpu:
				print("no such process.")
				exit(1)
			temp = get_temp()
			print("%.2f,%.2f,%s,%s" % (cpu, mem, temp, elapsedTime))
			time.sleep(1)
	except KeyboardInterrupt:
		print
		exit(0)

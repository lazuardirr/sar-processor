function unload(directory = "../../src/") 
    rmpath(directory);
    rmpath(strcat(directory, "reader"));
    rmpath(strcat(directory, "utils"));
    rmpath(strcat(directory, "../resources"));

    restoredefaultpath();
    clear work_dir;
endfunction
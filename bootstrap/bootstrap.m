function work_dir = bootstrap(directory = "../../src/")
    addpath(directory);
    addpath(strcat(directory, "reader"));
    addpath(strcat(directory, "utils"));
    addpath(strcat(directory, "../resources"));

    work_dir = directory;
endfunction